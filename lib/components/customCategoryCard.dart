import 'package:flutter/material.dart';
import 'package:newsapp/model/article_model.dart';
import 'package:newsapp/pages/webview_page.dart';
import 'package:share/share.dart';

class CustomCategoryCard extends StatelessWidget {
  final Article article;
  CustomCategoryCard({@required this.article});

  String removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => WebViewPage(article: article),
            ));
          },
          child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 200,
                      child: FadeInImage.assetNetwork(
                          placeholder: 'assets/images/default-banner.jpeg',
                          fit: BoxFit.fill,
                          imageErrorBuilder: (context, error, stackTrace) {
                            return Image.asset(
                                'assets/images/default-banner.jpeg',
                                fit: BoxFit.fitWidth);
                          },
                          image: article.url),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            removeAllHtmlTags(article.title),
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 15),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(),
                        Row(
                          children: [
                            Text(article.categoryName),
                            IconButton(
                              icon: Icon(Icons.share),
                              onPressed: () {
                                Share.share(
                                    article.title + " " + article.shortLink);
                              },
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              )),
        ));
  }
}
