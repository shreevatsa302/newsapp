import 'package:flutter/material.dart';
import 'package:newsapp/components/customCard.dart';
import 'package:newsapp/components/customCategoryCard.dart';
import 'package:newsapp/model/article_model.dart';
import 'package:newsapp/services/api_service.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CustomTabview extends StatefulWidget {
  final bool ishome;
  final String id;
  CustomTabview({@required this.ishome, @required this.id});
  @override
  _CustomTabviewState createState() => _CustomTabviewState();
}

class _CustomTabviewState extends State<CustomTabview>
    with AutomaticKeepAliveClientMixin<CustomTabview> {
  @override
  bool get wantKeepAlive => true;

  ApiService client = ApiService();
  List<Article> article = [];

  List<Widget> tabs = [];
  List<Widget> tabviews = [];

  RefreshController _refreshController;
  void _onRefresh() async {
    if (widget.ishome) {
      article = await client.getArticle();
    } else {
      article = await client.getArticleByCategory(widget.id);
    }
    if (mounted) setState(() {});
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    if (widget.ishome) {
      article = await client.getArticle();
    } else {
      article = await client.getArticleByCategory(widget.id);
    }
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    _refreshController = RefreshController(initialRefresh: true);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: MaterialClassicHeader(),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
          itemCount: article.length,
          itemBuilder: (context, index) => widget.ishome
              ? CustomCard(article: article[index])
              : CustomCategoryCard(article: article[index]),
        ));
  }
}
