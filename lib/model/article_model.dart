class Article {
  String title;
  String description;
  String url;
  String publishedAt;
  String publishedTime;
  String categoryName;
  int id;
  String shortLink;

  Article(
      {this.title,
      this.description,
      this.url,
      this.categoryName,
      this.publishedTime,
      this.shortLink,
      this.publishedAt,
      this.id});

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
        id: json['id'] as int ?? 0,
        title: json['title'] as String ?? "",
        description: json['description'] as String ?? "",
        url: json['featured_image'] as String ?? "",
        publishedAt: json['createDate'] as String ?? "",
        shortLink: json['long_url'] as String ?? "",
        publishedTime: json['createTime'] as String ?? "",
        categoryName: json['categoryName'] as String ?? "");
  }

  factory Article.categoryFromJson(Map<String, dynamic> json) {
    return Article(
        id: json['id'] as int ?? 0,
        title: json['title'] as String ?? "",
        description: json['description'] as String ?? "",
        url: json['image'] as String ?? "",
        publishedAt: json['created_at'] as String ?? "",
        publishedTime: json['created'] as String ?? "",
        shortLink: json['link'] as String ?? "",
        categoryName: json['categoryName'] as String ?? "");
  }
}
