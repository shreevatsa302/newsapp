class Source {
  int id;
  String name;

  Source({this.id, this.name});

  factory Source.fromJson(Map<String, dynamic> json) {
    print(json['name']);
    return Source(
        id: json['id'] as int ?? 0, name: json['name'] as String ?? "");
  }
}
