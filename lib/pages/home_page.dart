import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:newsapp/components/customTabview.dart';
import 'package:newsapp/model/article_model.dart';
import 'package:newsapp/model/source_model.dart';
import 'package:newsapp/services/api_service.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Article> article = [];

  List<Widget> tabs = [];
  List<Widget> tabviews = [];
  List<Source> category = [];
  ApiService client = ApiService();

  @override
  void initState() {
    initTab();
    super.initState();
  }

  void initTab() async {
    tabs.add(Tab(
      text: 'HOME',
    ));
    tabviews.add(CustomTabview(
      id: "",
      ishome: true,
    ));
    category = await client.getCategories();
    initailizeWidget(category);
    setState(() {});
  }

  void initailizeWidget(List<Source> cat) {
    tabs = [];
    tabviews = [];
    tabs.add(Tab(
      text: 'HOME',
    ));
    tabviews.add(CustomTabview(
      id: "",
      ishome: true,
    ));

    cat.forEach((e) {
      tabs.add(Tab(
        text: e.name.toUpperCase(),
      ));
      tabviews.add(CustomTabview(
        id: e.id.toString(),
        ishome: false,
      ));
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: tabs.length,
        child: new Scaffold(
          backgroundColor: Colors.black,
          appBar: new AppBar(
            title: new Text("Newsapp"),
            backgroundColor: Colors.red,
          ),
          body: Column(
            children: [
              Container(
                height: 45,
                width: double.infinity,
                child: TabBar(
                  labelPadding: EdgeInsets.symmetric(horizontal: 15.0),
                  isScrollable: true,
                  indicatorColor: Color(0xffF15C22),
                  unselectedLabelColor: Colors.white60,
                  labelColor: Color(0xffF15C22),
                  tabs: tabs,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TabBarView(children: tabviews),
                ),
              ),
            ],
          ),
        ));
  }
}
