import 'dart:io';

import 'package:flutter/material.dart';
import 'package:newsapp/model/article_model.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  final Article article;
  WebViewPage({@required this.article});
  @override
  WebViewPageState createState() => WebViewPageState();
}

class WebViewPageState extends State<WebViewPage> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(widget.article.categoryName),
        backgroundColor: Colors.red,
      ),
      body: SafeArea(
        child: WebView(
          initialUrl: widget.article.shortLink,
        ),
      ),
    );
  }
}
