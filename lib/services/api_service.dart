import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:newsapp/model/article_model.dart';
import 'package:newsapp/model/source_model.dart';

class ApiService {
  final endPointUrl = "admin-v2.lokwani.in";
  final client = http.Client();

  Future<List<Article>> getArticle() async {
    try {
      final uri = Uri.https(endPointUrl, '/api/general_news');
      final response = await client.get(uri);
      Map<String, dynamic> json = jsonDecode(response.body);
      List<dynamic> body = json['data'];
      List<Article> articles =
          body.map((dynamic item) => Article.fromJson(item)).toList();
      return articles;
    } catch (er) {
      return [];
    }
  }

  Future<List<Article>> getArticleByCategory(String id) async {
    try {
      final queryParameters = {
        '?rsscategory_id': id,
      };

      final uri = Uri.https(
        endPointUrl,
        '/api/rss-feeds',
        queryParameters,
      );
      print(uri.toString());
      final response = await client.get(uri, headers: {
        "Access-Control-Allow-Origin": "*",
      });
      print(response);

      Map<String, dynamic> json = jsonDecode(response.body);

      Map<String, dynamic> json2 = json['data'];

      List<dynamic> body = json2['data'];

      print(body);
      List<Article> articles =
          body.map((dynamic item) => Article.categoryFromJson(item)).toList();
      return articles;
    } catch (er) {
      print("error");
      return [];
    }
  }

  Future<List<Source>> getCategories() async {
    try {
      final uri = Uri.https(endPointUrl, '/api/rsscategories');
      final response = await client.get(uri);
      List<dynamic> body = jsonDecode(response.body);
      List<Source> source =
          body.map((dynamic item) => Source.fromJson(item)).toList();
      return source;
    } catch (er) {
      return [];
    }
  }
}
